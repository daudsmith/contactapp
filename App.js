import MainNavigator from './src/routers/navigator';
import { ContactProvider } from './src/context/contactContext'

const App = () => {
  return (
    <ContactProvider>
      <MainNavigator />
    </ContactProvider>
  );
};

export default App;
