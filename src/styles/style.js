import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginLeft: 5,
        marginRight: 5,
    },
      itemContainer: {
        marginTop: 30,
        flexDirection: 'row',
        height: 60,
        alignItems: 'center',
        padding: 2,
        backgroundColor: '#ecf0f1',
        marginLeft: -12,
      },
      searchInput: {
        marginTop: 10,
        height: 45,
        alignItems: 'center',
        padding: 2,
        backgroundColor: '#ecf0f1',
        borderWidth: 1,
        borderColor: '#95a5a6',
        borderRadius: 4,
      },
      hiddenItemContainer: {
        flex: 1, 
        flexDirection: 'row', 
        justifyContent: 'flex-end',
        marginTop: 30,
      },
      textContainer: {
        flexDirection: 'column',
      },
      placeholderText: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#ecf0f1',
      },
      title: {
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: 30,
        color: '#2c3e50',
      },
      subtitle: {
        fontSize: 18,
        marginLeft: 30,
        color: '#7f8c8d',
      },
      nodata: {
        fontSize: 22,
        fontWeight: 'bold',
        marginLeft: 120,
        marginTop: 250,
        color: '#bdc3c7',
      }
});
