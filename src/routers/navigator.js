import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import ListContact from '../screens/listContact';
import DetailContact from '../screens/detailContact';

const Stack = createStackNavigator();

const MainNavigator = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}} initialRouteName="List Contact">
        <Stack.Screen name="List Contact" component={ListContact}/>
        <Stack.Screen name="Detail Contact" component={DetailContact}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default MainNavigator;
