import React, { useEffect, useState } from 'react';
import { SafeAreaView, Text, View } from 'react-native';
import LoadingComponent from '../components/loadingComponent';
import HeaderComponent from '../components/headerComponent';
import { styles } from '../styles/style';


const DetailContact = ({ route }) => {
  const [isLoading, setIsLoading] = useState(true);
  const { contact } = route.params;

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    }, 500);
  }, []);

  if (isLoading) {
    return (
      <LoadingComponent/>
    );
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
        <HeaderComponent/>
        <View style={styles.container}>
          <View style={styles.itemContainer}>
            <View style={styles.textContainer}>
              <Text style={styles.title}>{contact.name}</Text>
              <Text style={styles.subtitle}>{contact.phone}</Text>
              <Text style={styles.subtitle}>{contact.email}</Text>
              <Text style={styles.subtitle}>{contact.website}</Text>
            </View>
          </View>
          <View style={styles.itemContainer}>
            <View style={styles.textContainer}>
              <Text style={styles.subtitle}>{contact.address.street}, {contact.address.suite}</Text>
              <Text style={styles.subtitle}>{contact.address.city}, {contact.address.zipcode}</Text>
            </View>
          </View>
          <View style={styles.itemContainer}>
            <View style={styles.textContainer}>
              <Text style={styles.title}>{contact.company.name}</Text>
              <Text style={styles.subtitle}>{contact.company.catchPhrase}</Text>
              <Text style={styles.subtitle}>{contact.company.bs}</Text>
            </View>
          </View>
        </View>
    </SafeAreaView>
  );
};
export default DetailContact;