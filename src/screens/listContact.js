import React, { useContext, useEffect, useState } from 'react';
import { SafeAreaView, TouchableOpacity, View, Text, TextInput } from 'react-native';
import { SwipeListView } from 'react-native-swipe-list-view';
import { ContactContext } from '../context/contactContext';
import LoadingComponent from '../components/loadingComponent';
import HeaderComponent from '../components/headerComponent';
import { styles } from '../styles/style';


const ListContact = ({ navigation }) => {
  const [isLoading, setIsLoading] = useState(true);
  const { data, setData } = useContext(ContactContext);
  const [searchKeyword, setSearchKeyword] = useState('');
  const [searchResults, setSearchResults] = useState([]);

  const handleDetail = (id) => {
    const contactToDetail = data.find((item) => item.id === id);
    if (contactToDetail) {
      navigation.navigate('Detail Contact', { contact: contactToDetail });
    }
  };

  const renderItem = ({ item }) => (
    <View style={styles.itemContainer}>
      <View style={styles.textContainer}>
        <Text style={styles.title}>{item.name}</Text>
        <Text style={styles.subtitle}>{item.phone}</Text>
      </View>
    </View>
  );

  const renderHiddenItem = (data) => (
    <View style={styles.hiddenItemContainer}>
      <TouchableOpacity style={{ backgroundColor: '#3498db', justifyContent: 'center', alignItems: 'center', width: 80 }} onPress={() => handleDetail(data.item.id)}>
        <Text style={{ color: 'white' }}>Detail</Text>
      </TouchableOpacity>
    </View>
  );

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    }, 2000);
  }, []);

  if (isLoading) {
    return (
      <LoadingComponent/>
    );
  }

const filteredData = searchKeyword
  ? data.filter((item) => item.name.toLowerCase().includes(searchKeyword.toLowerCase()))
  : data;

return (
  <SafeAreaView style={{ flex: 1 }}>
    <HeaderComponent />
    <View style={styles.container}>
      <TextInput
        style={styles.searchInput}
        placeholder="Search by name..."
        onChangeText={(text) => setSearchKeyword(text)}
        value={searchKeyword}
      />
      {filteredData.length > 0 ? (
        <SwipeListView
          data={filteredData}
          renderItem={renderItem}
          renderHiddenItem={renderHiddenItem}
          rightOpenValue={-95}
        />
      ) : (
        <View style={styles.textContainer}>
          <Text style={styles.nodata}>No matching contacts found</Text>
        </View>
      )}
    </View>
  </SafeAreaView>
);
};

export default ListContact;