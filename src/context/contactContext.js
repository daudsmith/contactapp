import React, { createContext, useState, useEffect } from 'react';

export const ContactContext = createContext();

export const ContactProvider = ({ children }) => {
  const [data, setData] = useState('');
  const [isLoading, setIsLoading] = useState(true);
  console.log(data)

  useEffect(() => {
    fetch('https://jsonplaceholder.typicode.com/users')
      .then((response) => response.json())
      .then((dataFromApi) => {
        setData(dataFromApi);
        setIsLoading(false);
      })
      .catch((error) => {
        console.error('Error fetching data:', error);
        setIsLoading(false);
      });
  }, []);

  return (
    <ContactContext.Provider value={{ data, setData }}>
      {children}
    </ContactContext.Provider>
  );
};
