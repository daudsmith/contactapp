import React from 'react';
import { View, Text, StyleSheet, ActivityIndicator } from 'react-native';

const LoadingComponent = () => {

    return (
      <View style={styles.container}>
        <ActivityIndicator size="large" color="#ecf0f1" />
        <Text style={styles.loadingText}>Contact App</Text>
      </View>
    );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#3498db'
  },
  loadingText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#ecf0f1',
    marginTop: 15,
  },
});

export default LoadingComponent;
