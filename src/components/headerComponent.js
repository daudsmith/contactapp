import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const HeaderComponent = () => {

    return (
        <View style={styles.header}>
            <Text style={styles.headerText}>Contact App</Text>
        </View>
    );
};

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#3498db',
    padding: 20,
    alignItems: 'center',
    marginBottom: 8,
    height: 100,
},
headerText: {
    color: '#fff',
    fontSize: 19,
    marginTop: 30,
    fontWeight: 'bold',
},
});

export default HeaderComponent;
